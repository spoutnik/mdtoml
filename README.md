# mdtoml
Simple `github-markdown` wrapper I wrote to quickly convert markdown files into html files. If no options are specified, it will assume your encoding is `utf-8` and use a custom CSS.

To install:
```
git clone https://github.com/jbman100/mdtoml
cd mdtoml && ./install.sh
```
Assumes `ruby` is installed on your system.

Usage:

    mdtoml [-s/-r] [-c css|-i icon| .. ] file1.md file2.md ..

Special features are:

- `-s | --save` : Save the entered options.
- `-r | --restore` : Restore last saved options.

Options include:

- `-h | --help` : Display this help.
- `-q | --quiet` : Don't preview the result.
- `-c | --css` : Specify a css file to use. The path you will enter will be copied as-is into the output file.
- `-i | --icon` : Specify a icon path to use.
- `-e | --encoding` : Specify an encoding to use.
- `-t | --title` : The generated page's title.
- `-o | --output` : Choose the output file. Only works if a single file is to be processed.

Result can be found <a href=http://skutnik.iiens.net/cours/IPI>here</a>.
