#!/bin/bash

if ! [ $(which ruby) ] ; then
    echo You do not have ruby installed !
    exit 1
fi

function check_for_gem() {
    gem=$(gem list | grep $1)

    echo "$1: $gem"
    if [ "$gem" != "" ] ; then
	echo "Gem $1 has been found"
    else
	echo -n "The gem $1 is not installed. Install ? [Y/n] "
	read answer

	if ! [ $answer == "n" ] ; then
	    gem install $1
	else
	    exit 1
	fi
    fi
}

check_for_gem commonmarker
check_for_gem github-markup

mkdir -p "$HOME"/.local/bin
cp ./mdtoml "$HOME"/.local/bin
chmod +x "$HOME"/.local/bin/mdtoml

echo
echo "mdtoml has been copied to /home/"$USER"/.local/bin/mdtoml"
echo "You may need to update your PATH. See:"
echo "https://wiki.archlinux.org/index.php/environment_variables"
echo
